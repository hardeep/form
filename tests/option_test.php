<?php

class Option extends PHPUnit_Framework_TestCase {

    function test_render() {
        $element = new Form\Elements\Option('CurrentUsername'); 
        $element->add_property('class', '\'form_text_input');
        $element->value = "12398a(*&DSA";

        $rendered = $element->render() . "\n";
        $expected = file_get_contents(
            FIXTURES_DIR . '/option/expected_render.html'
            );

        $this->assertEquals($rendered, $expected);
    }

}
