<?php

class ElementTest extends PHPUnit_Framework_TestCase {

    function test_render() {
        $element = new Form\Elements\PasswordTextInput('CurrentUsername'); 
        $element->add_property('class', '\'form_text_input');
        $element->value = "12398a(*&DSA";

        $expected = 
            file_get_contents(FIXTURES_DIR . "/password_input_render.html");

        $rendered = $element->render() . "\n";

        $this->assertEquals($expected, $expected);
    }
}
