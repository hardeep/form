<?php

class Option extends PHPUnit_Framework_TestCase {

    function test_render() {
        $select = new Form\Elements\Select('countries');

        $canada = new Form\Elements\Option('canada'); 
        $canada->value = "Canada";
        $select->add_child($canada);

        $canada = new Form\Elements\Option('usa'); 
        $canada->value = "United States of America";
        $select->add_child($canada);

        $rendered = $select->render() . "\n";
        $expected = file_get_contents(
            FIXTURES_DIR . "/select/basic_render.html"
            );

        $this->assertEquals($rendered, $expected);
    }
    
    function test_set_option() {
        $select = new Form\Elements\Select('countries');

        $canada = new Form\Elements\Option('canada'); 
        $canada->value = "Canada";
        $select->add_child($canada);

        $canada = new Form\Elements\Option('usa'); 
        $canada->value = "United States of America";
        $select->add_child($canada);

        $select->set_value('canada');

        $this->assertTrue(
            array_key_exists(
                'SELECTED',
                $select->get_child('canada')->get_properties()
                )
            );
    }
}
