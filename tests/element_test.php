<?php

class ElementTest extends PHPUnit_Framework_TestCase {

    function test_safe_name() {
        $element = new Form\Elements\Input('CurrentUsername'); 

        $this->assertEquals(
            $element->safe_name('form name'),
            'formName'
            );

        $this->assertEquals(
            $element->safe_name('form na& me &'),
            'formNaMe'
            );
    }

    function test_render() {
        $element = new Form\Elements\Input('CurrentUsername'); 
        $element->add_property('class', '\'form_text_input');
        $element->value = "12398a(*&DSA";

        $expected = 
            file_get_contents(FIXTURES_DIR . "/text_input_render.html");

        $rendered = $element->render();

        $this->assertEquals($rendered, $expected);
    }

    function test_validator() {
        $element = new Form\Elements\Input('CurrentUsername'); 
        $element->value = "shokerhardeep@gmail.com";

        $element->add_validator(function($value) {
            return filter_var( 
                $value, FILTER_VALIDATE_EMAIL, FILTER_NULL_ON_FAILURE
                );
        });

        $this->assertTrue($element->is_valid());
    }

    function test_child_selector() {
        $parent = new Form\Elements\Select('first_name');
        $child = new Form\Elements\Option('some_name');
        $parent->add_child($child);
        $this->assertEquals($parent->some_name->safe_name, 'someName');
        $this->assertEquals($parent->some_Name->safe_name, 'someName');
        $this->assertEquals($parent->someName->safe_name, 'someName');
        $this->assertEquals($parent->SomeName->safe_name, 'someName');
    }
}
