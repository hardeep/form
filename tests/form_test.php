<?php

class FormTest extends PHPUnit_Framework_TestCase {

    function test_new_class() {
        $form = new Form\Form();
    }

    function setUp() {
        $this->form = new Form\Form();
    }

    function test_add_element() {
        $this->form->add_element(new Form\Elements\Input('newElement'));
        $this->form->add_element(new Form\Elements\Select('countries'));

        $canada = new Form\Elements\Option('canada'); 
        $canada->value = "Canada";
        $this->form->add_child_element($canada);

        $usa = new Form\Elements\Option('usa'); 
        $usa->value = "United States of America";
        $this->form->add_child_element($usa);

        $expected = file_get_contents(FIXTURES_DIR . "/form_expected_render.html"); 
        $result = $this->form->render() . "\n";

        $this->assertEquals($expected, $result);
    }

    function test_bind_to_post() {
        $form = new Form\Form();
        $form->add_child(new Form\Elements\Input('client_name'));
        $_POST['client_name'] = "John Doe";
        $_POST['non_existant_feild'] = "something";
        $form->bind($_POST);
        $this->assertEquals($form->client_name->value, 'John Doe');
    }

    function test_is_valid() {
        $form = new Form\Form();

        $form->add_property('action', 'http://localhost');
        $form->add_property('method', 'post');

        $username = new Form\Elements\Input('username');
        $username->add_validator(function($email) {
                return filter_var( 
                    $email, FILTER_VALIDATE_EMAIL, FILTER_NULL_ON_FAILURE
                    );
            });

        $form->add_element($username);
        $form->add_element(new Form\Elements\PasswordTextInput('password'));

        $_POST['username'] = '#example.com';
        $_POST['password'] = 'temp123';

        $form->bind($_POST);

        $form->is_valid();
        $errors = $form->get_errors();

        $this->assertTrue(array_key_exists('username', $errors));
        $this->assertEquals(sizeof($errors), 1);
    }
}
