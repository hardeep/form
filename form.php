<?php 

require_once 'lib/auto_loader.class.php';
require_once 'lib/template.class.php';
require_once 'lib/elements/element.class.php';
require_once 'lib/elements/input.class.php';
require_once 'lib/elements/email_text_input.class.php';
require_once 'lib/elements/password_text_input.class.php';
require_once 'lib/elements/option.class.php';
require_once 'lib/elements/select.class.php';
require_once 'lib/form.class.php';

defined('FIXTURES_DIR') or 
    define('FIXTURES_DIR', dirname(__FILE__) . "/tests/fixtures");
