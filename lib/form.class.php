<?php

namespace Form {

use \InvalidArgumentException;

class Form extends Elements\Element {

    protected $selected = null;
    protected $last_element = null;
    protected $errors = array();
    static public $QOUTE_CHAR = "'";

    function __construct() {

    }

    function normalize() {}

    function get_errors() {
        return $this->errors;
    }

    function set_value($value) {
        return $this;
    }

    function add_element($element) {
        $this->add_child($element);
        $this->last_element = $element->safe_name;
        return $this;
    }

    function add_child_element($child_element) {
        $element = &$this->children[$this->last_element];
        $element->add_child($child_element);
        return $this;
    }

    function bind($array) {
        $safe_bind = null;

        if (gettype($array) != 'array') {
            if (method_exists($array, 'to_array')) {
               $safe_bind = $array->to_array(); 
            } else {
                throw InvalidArgumentException(
                    "Object does not support to_array"
                    );
            }
        } else {
            $safe_bind = $array;
        }

        foreach ($safe_bind as $key => $value) {
            echo "$key => $value\n";
            if ($this->has_child($key)) {
                $this->{$key} = $value;
            }
        }
    }

    function is_valid() {
        $this->errors = array();
        foreach ($this->children as $element) {
            if (!$element->is_valid()) {
                $this->errors[ $element->safe_name ] = $element;
            }
        }

        return (sizeof($this->errors) > 0) ? false : true;
    }

    function get_escape_char() {
        return (Form::$QOUTE_CHAR == "'") ? "\'" : '\"' ;
    }

    function render() {
        $output = "<form " . $this->properties_to_s() . " >\n";
        $output .= Form::render_down($this->children);
        return $output . "</form>";
    }

} # end Form

} # end Form
