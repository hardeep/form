<?php

namespace Form\Elements {

use \Exception;
use \Form\AutoLoader;

abstract class Element {
    
    # Element attributes
    public $name = null;
    public $value = null;
    public $safe_name = null;
    public $id = null;

    protected $properties = array();

    protected $validator_chain = array();
    protected $children = array();

    static $QOUTE_CHAR = "'";
    
    function __construct($name) {
        $this->name = $name;
        $this->safe_name = Element::safe_name($name);
        $this->id = $name;
    }

    function get_properties() {
        return $this->properties;
    }

    function get_child($name) {
        if (array_key_exists($name, $this->children)) {
            return $this->children[$name];
        }

        throw new Exception("$name does not exist in " . $this->safe_name);
    }

    function get_children() {
        return $this->children;
    }

    function add_child($element) {
       $this->children[ $element->safe_name($element->name) ] = $element;
    }

    function add_validator($method, $class = null) {
        array_push($this->validator_chain, array( $method, $class));
    }

    # abstract to set the value when parsing a form

    abstract function set_value($value);

    function has_children() {
        return ( sizeof($this->children) > 0 ) ?
            true :
            false;
    }

    function has_child($child_name) {
        $safe_name = Element::safe_name($child_name);
        if (array_key_exists($safe_name, $this->children)) {
            return true;
        } else {
            return false;
        }
    }

    ## Safe functions

    static function safe_name($name) {
        $name = AutoLoader::to_camel_case($name);
        $name = preg_replace("/[^a-zA-Z0-9]/", '', $name);
        return lcfirst($name);
    }

    static function safe_id($name) {
        $name = AutoLoader::to_underscore_case($name);
        $name = preg_replace("/[^a-zA-Z0-9_]/", '', $name);
        return $name;
    }

    function safe_value() {
        $escape_char = $this->get_escape_char();
        $value =  htmlspecialchars($this->value);
        $value = str_replace(
                Element::$QOUTE_CHAR, $escape_char, $value
                );

        return $value;
    }

    ## Element attribute functions

    function add_property($key, $value) {
        $this->properties[$key] = array($key, $value);
    }

    abstract function normalize();

    function is_valid() {
        foreach ($this->validator_chain as $validator) {
            list( $method, $class) = $validator; 

            if ($class) {
                if (method_exists($class, $method)) {
                    if (!$class->{$method}($this->value)) {
                        return false;
                    }
                }
                # the class method doesn't exist fail
                return false;
            } else {
                if (!$method($this->value)) return false; 
            }
        }

        return true;
    }

    ## Render functions

    function get_escape_char() {
        return (Element::$QOUTE_CHAR == "'") ? "\'" : '\"' ;
    }

    # abstract method to override to render this element
    abstract function render();

    static function render_down($children, $tabs = 1) {
        $rendered_text = "";
        foreach($children as $child) {
            $rendered_text .= $child->render() . "\n";
        }
        return $rendered_text;
    }

    function properties_to_s() {
        $escape_char = $this->get_escape_char();

        $properties = "";

        foreach ($this->properties as $pair) {
            list($key, $value) = $pair;
            $properties .= 
                "$key=" . 
                Element::$QOUTE_CHAR .
                str_replace(
                    Element::$QOUTE_CHAR, $escape_char, $value
                    ) . 
                Element::$QOUTE_CHAR. " ";
        }

        return $properties;
    }

    function __toString() {
        return $this->render();
    }

    function __get($child) {
        $child_name = Element::safe_name($child);
        if (array_key_exists($child_name, $this->children)) {
            return $this->children[$child_name];
        }
        return null;
    }

    function __set($child, $value) {
        $child_name = Element::safe_name($child);
        if (array_key_exists($child_name, $this->children)) {
            $this->children[$child_name]->set_value($value);
            return $this;
        }
        throw new Exception("child $child not found in form object");
    }

} # end Element

} # end Form
