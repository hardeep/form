<?php

namespace Form\Elements {

class Input extends Element {

    protected $type="text";

    function render() {
        return "<input " .
            "type='" . $this->type . "' " .
            "id='" . Element::safe_id($this->name) . "' " .
            "name='" . Element::safe_name($this->name) . "' ".
            $this->properties_to_s() . 
            "value='" . $this->normalize() . "' />\n";
    }

    function set_value($value) {
        $this->value = $value;
    }

    function normalize() {
        return $this->safe_value();
    }

} # end InputElement 

} # end Form
