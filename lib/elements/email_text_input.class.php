<?php

namespace Form\Elements {

class EmailTextInput extends Input {

    function __construct($name) {
        parent::__construct($name);

        $this->add_validator(function($value) {
            return filter_var( 
                $value, FILTER_VALIDATE_EMAIL, FILTER_NULL_ON_FAILURE
                );
        });
    }

} # end EmailTextInput

} # end Form
