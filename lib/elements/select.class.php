<?php

namespace Form\Elements {

class Select extends Element {

    protected $selected_option = null;

    function render() {
        return "<select " .
            "id='" . Element::safe_id($this->name) . "' " .
            "name='" . Element::safe_name($this->name) . "' ".
            $this->properties_to_s() . 
            ">\n" . Select::render_down($this->children) . "</select>";
    }

    function get_options() {
        return $this->children;
    }

    function set_value($value) {
        $safe_name = $this->safe_name($value);
        if (!$this->has_child($safe_name)) {
            throw new Exception("option $value not found");
        }
        $this->selected_option = $safe_name;
        $this->children[ $safe_name ]->set_selected();
    }

    function normalize() {
        return $this->selected_option;
    }

} # end Select 

} # end Form
