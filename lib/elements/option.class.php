<?php

namespace Form\Elements {

class Option extends Element {

    function render() {
        return "<option " .
            "id='" . Element::safe_id($this->name) . "' " .
            "name='" . Element::safe_name($this->name) . "' ".
            $this->properties_to_s() . 
            ">" . $this->normalize() . "</option>";
    }

    function set_value($value) {
        return;
    }

    function set_selected() {
        $this->add_property('SELECTED', 'TRUE');
    }

    function normalize() {
        return $this->safe_value();
    }

} # end Option 

} # end Form
