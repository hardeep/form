<?php

namespace Form\Elements {

class PasswordTextInput extends Input {

    protected $type = 'password';

    function render() {
        $this->value = "";
        return parent::render();
    }

} # end PasswordTextInput

} # end Form
