<?php

namespace Form {

class Template {

    private $var_regex = 
        "/\{\{
        [ \t\r\n]*
        [a-zA-Z0-9_]+
        [\.
            [a-zA-Z0-9_]+
            (:?\(\)){0,1}
        ]*
        \?{0,1}
        [ \t\r\n]*
        \}\}
        /xm";

    function __construct(&$args = null) {
        $this->args = &$args;
        $this->stackLevel = 0;
        $this->callStack = array();
    }

    function load_template_file($filename) {
        if (file_exists($filename) && is_readable($filename)) {
            $page = "";
            $fileHandle = fopen($filename, "r");
            while (!feof($fileHandle)) {
                $page .= fgets($fileHandle);
            }

            $this->page = &$page;

            return true;
        } else {
            throw new FileException("Can't find the template file $filename");
        }
    }

    private function init_vars() {
        if ($this->args == null) return;
        foreach ($this->args as $key => &$value) {
            $this->vars[$key] = array("value" => &$value, "stackCursor" => 0);
        }
    }

    private function process_callstack(&$page)  { 
        # push blocks of code, segment into a stack, page gets destroyed
        $currentPos = 0;
        $newPos = strpos($page, "{%");

        if ($newPos !== false) {
            # get the code before the logic block
            $sub = substr($page, 0, $newPos); 

            # place it on the call stack
            array_push(
                    $this->callStack,
                    array("section" => $sub, "level" => $this->stackLevel)
                );

            $currentPos = $newPos; # get the starting section of the logic code block
            $newPos = strpos($page, "%}") + 2; # get the ending section of the logic code block

            $length = $newPos - $currentPos;
            $sub = substr($page, $currentPos, $length); # get the code block

            # the remaining of the page will be stored for further processing 
            $page = substr($page, $currentPos + $length); 

            # if it's a endif or endfor add it to the call stack but decrement the
            # call stack level 
            if (preg_match("/\{\%[ \t\r\n]*end(if|for)[ \t\r\n]*\%\}/", $sub)) {
                $this->stackLevel--;

                array_push(
                        $this->callStack,
                        array("section" => $sub, "level" => $this->stackLevel)
                    );
            }
            # if it's a template the add it to the callstack with the current
            # callstack level
            else if (
                    preg_match(
                            "/\{\%[ \t\r\n]*template[ \t\r\n]*[a-zA-Z0-9_]+[ \t\r\n]*\%\}/",
                            $sub
                        )
                ) {

                array_push(
                        $this->callStack,
                        array("section" => $sub, "level" => $this->stackLevel)
                    );

            }
            # otherwise add it to the current callstack level and then incrememnt it
            # this is needed because we assume we have hit a 'if' for 'for' block
            # hence we increment the callstack level
            else {
                array_push(
                        $this->callStack,
                        array("section" => $sub, "level" => $this->stackLevel)
                    );
                $this->stackLevel++;
            }

            # process the rest of the page
            $this->process_callstack($page);
        } 
        # we found a piece of code that does not contain any logic
        else {
            array_push(
                    $this->callStack,
                    array("section" => $page, "level" => $this->stackLevel)
                );
        }
    }

    private function get_var_value($var) {
            # remove any padding 
            $var = trim($var);

            # could be a array so lets explode the lookup
            $var = explode('.', $var);

            # get the first variable
            $index = array_shift($var);
            if (empty($this->vars[$index])) return null;

            if (end($var) == 'last?') {
                return $this->vars[$index]['last?'];
            }

            $replace = $this->vars[$index]['value']; 
        
            # itterate through each dictionary lookup the user
            # requested
            foreach($var as $sub_index) {
                # the user may have provided an object
                $is_object = is_object($replace);
                $is_array = is_array($replace);

                # can we convert it to an array?
                # this helps with the edge case that the user has used
                # __get or __set magic methods
                # the edge case occurs when we try and test if the method
                # or attribute exists, with magic methods the *_exists
                # functions fail
                if (method_exists($replace, 'to_array')) {
                    $replace = $replace->to_array();
                    $is_object = false;
                }
                
                if ($is_array) {
                    # check for variable functions 
                    if (preg_match(
                            "/^has_key_([^ ]*)/",
                            $sub_index,
                            $has_var)
                        ) {
                        if (isset($replace[$has_var[1]])) {
                            return true;
                        }
                        else {
                            return false;
                        }
                    }

                    if ($sub_index == 'join()') {
                        return join(",", $replace);
                    } 
                    else if ($sub_index == 'php_array()') {
                         ob_start();
                         var_export($replace, false);
                         $ret = ob_get_contents();
                         ob_end_clean();
                         return $ret;
                    }
                }


                # if it's an object or the index doesn't exist
                if ($is_object)  {
                    # test if the method exists
                    if ($is_object && method_exists($replace, $sub_index)) {
                        $replace = $replace->$sub_index();
                        continue;
                    }

                    # test if the attribute exists
                    if ($is_object && property_exists($replace, $sub_index)) {
                        $replace =  $replace->$sub_index;
                        continue;
                    }

                    # we have nothing to work just try and hope
                    # they have a magic method
                    $replace = $replace->{$sub_index};
                    continue;
                } 
                
                if (!isset($replace[$sub_index])) {
                    return null;
                }

                $replace = $replace[$sub_index];
            }

            return $replace;
    }

    private function process_string_vars(&$section) {
        $matches;

        preg_match_all(
            $this->var_regex, 
            $section, 
            $matches
        );

        $original = $matches;

        # foreach matched variable try and do a dictionary lookup
        foreach ($matches[0] as $variable) {
            $var = str_replace('{{', '', $variable);
            $var = str_replace('}}', '', $var);

            $replace = $this->get_var_value($var);
            $replace = ($replace === null)?"":$replace;
            if (is_array($replace)) $replace = json_encode($replace);

            $section = str_replace($variable, $replace, $section);
        }

        return true;
    }

    private function process_foreach_vars(&$foreach) {
        $var = $foreach[2];
        $base = $this->get_var_value($var);
        return $base;
    }

    private function process_if_not_var($var)  {
        $base = $this->get_var_value($var);
        if ($base == false)
            return true;
        return false;
    }

    private function process_if_var($var) {
        $base = $this->get_var_value($var);

        if ($base == true)
            return true;
        return false;
    }

    private function process_template_location(&$section) {

        $ifvar = explode('.', $section);
        $base = $this->vars[$ifvar[0]]["value"];

        array_shift($ifvar);

        foreach ($ifvar as $key => $value) {
            $base = $base[$value];
        }

        return $base;
    }

    private function parse_callstack($stackPos, $targetPos) {
        if ($stackPos >= $targetPos) {
            $codeblock = $this->callStack[$stackPos]["section"];
            #  process the variables in the codeblock
            $this->process_string_vars($codeblock);
            echo $codeblock;
            return;
        }
        else {
            $codeblock = $this->callStack[$stackPos];

            if (preg_match("/\{\%(.|\s)*\%\}/", $codeblock["section"])) {
                if (
                    preg_match(
                        "/\{\%[ \t\r\n]*
                        foreach
                        [ \t\r\n]*
                        ([a-zA-Z0-9_]+[\.[a-zA-Z0-9_]+]*)
                        [ \t\r\n]*
                        in
                        [ \t\r\n]*
                        ([a-zA-Z0-9_]+[\.[a-zA-Z0-9_]+]*)
                        [ \t\r\n]*\%\}/x",
                        $codeblock["section"],
                        $foreach
                        )
                    )  {
                    # this provides two functions to capture a foreach block
                    # as well as place the foreach's variables into the variable look up table

                    $endOfBlock = $stackPos + 1; #  enter the loop

                    # look into the callstack to determine how many code block belong to the
                    # foreach block
                    while (
                        $endOfBlock < sizeof($this->callStack)
                            && $this->callStack[$endOfBlock]["level"] != $codeblock["level"]
                    )  {
                        $endOfBlock++;
                    }

                    $set = $this->process_foreach_vars($foreach);

                    if (is_array($set)) {
                        $key_count = 0;

                        foreach ($set as $key => $tempValue) {
                            $key_count = $key_count + 1;

                            $this->vars[$foreach[1]] = array(
                                    "value" => &$tempValue,
                                    "stackCursor" => $codeblock["level"]
                                    );


                            if ($key_count == sizeof($set)) {
                                $this->vars[$foreach[1]]['last?'] = true;
                            }
                            else {
                                $this->vars[$foreach[1]]['last?'] = false;
                            }
                            $this->vars[$foreach[1]]['key?'] = $key;

                            $this->parse_callstack($stackPos + 1, $endOfBlock - 1);
                        }


                    }
                    return $this->parse_callstack($endOfBlock + 1, $targetPos);
                }
                else if (
                    preg_match(
                            "/\{\%[ \t\r\n]*
                            template[ \t\r\n]*
                            ([a-zA-Z0-9_]+[\.[a-zA-Z0-9_]+]*)
                            [ \t\r\n]*\%\}/x",
                            $codeblock["section"],
                            $templateName
                        )
                ) {
                    array_shift($templateName);

                    $location = $this->process_template_location($templateName[0]);

                    if (isset($location) && !empty($location)) {
                        $templateLocation = $location;
                        $template = new Template($this->args);
                        $template->load_template_file($templateLocation);
                        $template->render();
                        return $this->parse_callstack($stackPos + 1, $targetPos);
                    }
                }
                else if (
                    preg_match(
                            "/\{\%[ \t\r\n]*
                            !if
                            [ \t\r\n]*
                            ([a-zA-Z0-9_]+[\.[a-zA-Z0-9_]+]*\?{0,1})
                            [ \t\r\n]*\%\}/x",
                            $codeblock["section"],
                            $condition
                        )
                ) {
                    array_shift($condition);

                    $variableToEval = $condition[0];
                    $endOfBlock = $stackPos + 1; #  enter the loop

                    while (
                            $endOfBlock < sizeof($this->callStack) 
                                && $this->callStack[$endOfBlock]["level"] != $codeblock["level"]
                          ) {
                        $endOfBlock++;
                    }

                    if ($this->process_if_not_var($variableToEval)) {
                        $this->parse_callstack($stackPos + 1, $endOfBlock - 1);
                    }

                    return $this->parse_callstack($endOfBlock + 1, $targetPos);
                }
                else if (
                    preg_match(
                        "/\{\%[ \t\r\n]*
                        if[ \t\r\n]*
                        ([a-zA-Z0-9_]+[\.[a-zA-Z0-9_]+]*\?{0,1})
                        [ \t\r\n]*\%\}/x",
                        $codeblock["section"],
                        $condition
                    )
                ) {
                    array_shift($condition);

                    $variableToEval = $condition[0];
                    $endOfBlock = $stackPos + 1; #  enter the loop

                    while (
                        $endOfBlock < sizeof($this->callStack)
                            && $this->callStack[$endOfBlock]["level"] != $codeblock["level"]
                    ) {
                        $endOfBlock++;
                    }

                    if ($this->process_if_var($variableToEval)) {
                        $this->parse_callstack($stackPos + 1, $endOfBlock - 1);
                    }

                    return $this->parse_callstack($endOfBlock + 1, $targetPos);

                }
                else if
                (
                    preg_match(
                        "/\{\%[ \t\r\n]*
                        form
                        [ \t\r\n]*
                        ([a-zA-Z0-9_]+[\.[a-zA-Z0-9_]+]*)
                        [ \t\r\n]*\%\}/x",
                        $codeblock["section"],
                        $formName
                    )
                )  {
                    array_shift($formName);
                    
                    $form = $formName[0];
                      
                    if (isset($this->vars[$form])) {

                        if (method_exists($this->vars[$form]['value'], 'render')) {
                            $this->vars[$form]['value']->render();
                        }

                        return $this->parse_callstack($stackPos + 1, $targetPos);

                    } # @todo take care of undefined forms
                }
            }

            $codeblock = $this->callStack[$stackPos]["section"];

            #  process the variables in the codeblock
            $this->process_string_vars($codeblock);
            echo $codeblock;
            #echo $codeblock;

            return $this->parse_callstack($stackPos + 1, $targetPos);
        }
    }

    function tokenize_and_strip($string) {
      return rtrim($string);
    }

    public function render($options = null) {
        $this->init_vars();

        $this->process_callstack($this->page);

        try {

            if (!isset($options['post_process'])) {
                $this->parse_callstack(0, count($this->callStack) - 1);
            }
            else {
                ob_start();
                $this->parse_callstack(0, count($this->callStack) - 1);
                $rendered_template = ob_get_contents();
                ob_end_clean();
                foreach ($options['post_process'] as $process) {
                    $rendered_template = $process($rendered_template);
                }

                echo $rendered_template;
            }

        } catch (Exception $e) {
            throw new Exception("Could not parse stack:".$e);
        }
    }

    function render_as_string($options = null) {
        ob_start();
        $this->render($options);
        $rendered_template = ob_get_contents();
        ob_end_clean();
        return $rendered_template;
    }

    static function compact_multilines() {
        $filter = function($text) {
            $text = preg_replace('/^\n+|^[\s]*\n+/m',"",$text); 
            return $text;
        };

        return $filter;
    }

    static function comma_on_previous_line() {
        $filter = function($text) {
            $text = preg_replace('/\n[\s]*?,/m',",",$text); 
            return $text;
        };

        return $filter;
    }

    # the page template
    private $page;

    # input arguments
    private $args;

    # execution callstack
    private $callStack;

    # handles stack levels for the execution callstack
    private $stackLevel;

    # handle all variables
    private $vars;

} # end Template

} # end Form
