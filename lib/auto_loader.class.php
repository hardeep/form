<?php

namespace Form {

class AutoLoader {

  static $lib_folders;
  static $should_throw = true;

  public function __construct($should_throw = true) {
    AutoLoader::$lib_folders = array();
    spl_autoload_register('AutoLoader::load_libraries');
    AutoLoader::$should_throw = $should_throw;
  }

  public function register_libraries($directories, $prefix = "") {
    if (!is_array($directories)) {
        $temp = array($directories);
        $directories = $temp;
    }

    foreach ($directories as $dir) {
      array_push(AutoLoader::$lib_folders, $prefix . $dir);
    }
  }

  static function class_to_path($class) {
    $class = AutoLoader::to_underscore_case($class);
    $class = str_replace('\\', '/', $class); # change namespace to a path
    return $class;
  }

  static function load_libraries($class) {
    $class = AutoLoader::class_to_path($class);

    if (!is_array(AutoLoader::$lib_folders) || count(AutoLoader::$lib_folders) <= 0) {
      if ($this->should_throw) AutoLoader::throw_class_not_found($class);
    } 

    foreach (AutoLoader::$lib_folders as $dir) {
      $extensions = array("class.php", "php", "schema.class.php");

      foreach ($extensions as $ext) {
          if (file_exists($dir."/".$class.".$ext")) {
            include_once $dir."/".$class.".$ext";
            return true;
          }
      }
    }

    if (AutoLoader::$should_throw) AutoLoader::throw_class_not_found($class);
  }

  static public function registered_libraries() {
    return AutoLoader::$lib_folders;
  }

  static public function to_underscore_case($class) {
    if (preg_match_all("/([A-Z][^A-Z]*)/", $class, $results)) {
      array_shift($results);
      $class = join("_", $results[0]);
      $class = strtolower($class);
      $class = str_replace('\\_', '\\' ,$class);
      $class = str_replace('__', '_' ,$class);
    }

    return $class;
  }

  static public function to_camel_case($class_name) {
    $exp_array = explode(" ", $class_name);

    foreach ($exp_array as &$value) {
      $value = ucwords($value);
    }

    $class_name = join("", $exp_array);

    $exp_array = explode("_", $class_name);

    foreach ($exp_array as &$value) {
      $value = ucwords($value);
    }

    $class_name = join("", $exp_array);

    return $class_name;
  }

  static function throw_class_not_found($class) {
      throw new ClassNotFoundException($class.' was not found');
  }

  static function class_name($class_name) {
      $class = explode('\\', $class_name);
      return end($class); 
  }

} # end AutoLoader

} # end Form

